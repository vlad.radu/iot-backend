const awsIot = require('aws-iot-device-sdk');
const awsiot = require("./dummyconfig").awsiot;
var device = awsIot.device(awsiot);

console.log(device);

function random (min, max) {
    return (Math.random() * (max - min) + min).toFixed(2);
}

devices = ["esp", "esp2", "esp3", "esp4"];

devices.forEach(dev => {
    setInterval(function () {
        devId = dev;
        topic = `${devId}/temp`;
        data = {
            id: devId,
            temp: random(27, 31),
            timestamp: + new Date()
        };
        device.publish(topic, JSON.stringify(data));
        console.log("Published temp of " + data.temp + " on topic " + topic);
    }, 60000 * random(1,3));     
});
// setInterval(function () {
//     devId = 'esp';
//     topic = `${devId}/temp`;
//     data = {
//         id: devId,
//         temp: random(27, 31),
//         timestamp: + new Date()
//     };
//     device.publish(topic, JSON.stringify(data));
// }, 3000); 

