# Topics and JSON schema

# Publish

## device/startup
``` javascript
    {
        "id" : string,
        "timestamp" : string,
        "location" : string
        "locationIndices" : {
            "0" : ["temp", "prox", "led"],
            "1" : ["temp", "led"],
            ...
        }
    }
```
## device/location/locationIndex/led
``` javascript
    {
        "state": [0,0,0],
        "user": string
    }
```

## device/location/locationIndex/temp
``` javascript
    {
        "pollRate": float,
        "user": string
    }
```

## device/location/locationIndex/prox
``` javascript
    {
        "armed": bool,
        "user": string
    }
```


# Subscribe

## device/location/locationIndex/temp/index

``` javascript 
    {
        "id" : String,
        "timestamp" : int,
        "temp" : float,
        "pressure" : float,
        "wifi_strength" : float
    }
```

## device/location/locationIndex/led/index

``` javascript
    {
        "id" : String,
        "timestamp" : float,
        "state" : [0,0,0],
        "source" : String,
        "wifi_strength" : float
    }
```

## device/location/locationIndex/prox/index

``` javascript
    {
        "id" : String,
        "timestamp" : float,
        "triggered" :  bool,
        "distance" : float,
        "alarm" : bool,
        "wifi_strength" : float
    }
```
