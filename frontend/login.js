
document.addEventListener('DOMContentLoaded', main);

function main() {
    let sessionCheck = getCookie("sessionToken");
    if (sessionCheck){
        let pack = {
            token: sessionCheck
        }
        $.ajax({
            url: "/api/validate",
            type: "POST",
            data: JSON.stringify(pack),
            dataType: "json"
        }).done((data, status, xhr) => {
            console.log("valid token, redirecting...");
            redirectSameLevel("dashboard.html");
        }).fail((data, status, xhr) => {
            console.log("failed with", xhr);
            $("#status").text("Token expired or invalid: " + xhr);
            setListener();
        });
    }else {
        setListener();
    }
}

function setListener() {
    $("#login").click(function () {
        let button = this;
        button.disabled = true;
        let form = {};
        form["username"] = $("#username").val();
        form["password"] = $("#password").val();
        $.ajax({
            url: "/api/auth",
            type: "POST",            
            data: JSON.stringify(form),
            dataType: "json",
            complete: () => {
                button.disabled = false;
            }
        }).done((data, status, xhr) => {
            console.log(data);
            localStorage.setItem("username",data.username);
            setCookie("sessionToken", data.sessionToken);
            console.log(xhr.status);
            redirectSameLevel("dashboard.html");
        });
    });
}