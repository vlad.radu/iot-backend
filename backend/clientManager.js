const USERS_TABLE = "iotusers";
const userKey = 'cognito:username';
class ClientManager {
    constructor(docClient) {
        this.clients = {};
        this.docClient = docClient;
    }

    async saveClient(decodedJwt, socket) {
        let username = decodedJwt.payload[userKey];

        let params = {
            TableName: USERS_TABLE,
            FilterExpression : 'contains(username, :user)',
            ExpressionAttributeValues : { ':user': username}
        }
        let data = await scan(this.docClient, params);
        if (!this.clients[username]) {
            let client = {devices: data.Items[0].devices, sockets: []} ; 
            this.clients[username] = client;
        }
        else {
            this.clients[username].devices = data.Items[0].devices;
        }
        if (socket){
            this.clients[username].sockets.push(socket);
        }
    }

    get(username) {
        return this.clients[username];
    }

    forEach(func){
        Object.entries(this.clients).forEach( e => func(e[1]) );
    }

    async updateClientData(username) {
        let params = {
            TableName: USERS_TABLE,
            FilterExpression : 'contains(username, :user)',
            ExpressionAttributeValues : { ':user': username}
          }
          let data = await scan(this.docClient, params);
          this.get(username).devices = data.Items[0].devices;
    }
}

function scan(docClient, params ) {
    return new Promise((resolve, reject) => {
        docClient.scan(params, (err, data) => {
            if (err)
                reject(err);
            else
                resolve(data);
        })
    });
}
module.exports.scan = scan;
module.exports.ClientManager = ClientManager;
