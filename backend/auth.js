global.fetch = require("node-fetch");
global.navigator = () => null;
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const request = require("request");
const jwkToPem = require("jwk-to-pem");
const jwt = require("jsonwebtoken");

const poolData = require("./config").cognito;
const poolRegion = require("./config").cognitoRegion;

const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

var keys;

request({
    url : `https://cognito-idp.${poolRegion}.amazonaws.com/${poolData.UserPoolId}/.well-known/jwks.json`,
    json : true
 }, function(error, response, body){
    if (!error && response.statusCode === 200) {
        keys = body['keys'];
        pems = {};
        // var keys = body['keys'];
        for(var i = 0; i < keys.length; i++) {
            var key_id = keys[i].kid;
            var modulus = keys[i].n;
            var exponent = keys[i].e;
            var key_type = keys[i].kty;
            var jwk = { kty: key_type, n: modulus, e: exponent};
            var pem = jwkToPem(jwk);
            pems[key_id] = pem;
        }
        console.log("JWKs downloaded successfully!");
    } else {
          console.log("Error! Unable to download JWKs");
          return -1;
    }
});

module.exports.Login = function (body, callback) {
    var userName = body.username;
    var password = body.password;
    var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
         Username: userName,
         Password: password
     });
     var userData = {
         Username: userName,
         Pool: userPool
     }
     var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
     cognitoUser.authenticateUser(authenticationDetails, {
         onSuccess: function (result) {
            var accesstoken = result.getIdToken();
            // console.log(accesstoken);
            callback(null, accesstoken);
         },
         onFailure: (function (err) {
            callback(err);
        }),
        newPasswordRequired : function (userAttributes) {
            delete userAttributes.email_verified;
            cognitoUser.completeNewPasswordChallenge(password, userAttributes, this);
        }
    })
 };

 
 exports.Validate = function(token, callback){
    var decodedJwt = jwt.decode(token, {complete: true});
     if (!decodedJwt) {
         console.log("Not a valid JWT token");
         return callback(new Error('Not a valid JWT token'));
     }
     var kid = decodedJwt.header.kid;
     var pem = pems[kid];
     if (!pem) {
         console.log('Invalid token');
         callback(new Error('Invalid token'));
     }
    jwt.verify(token, pem, function(err, payload) {
         if(err) {
             console.log("Invalid Token.");
             callback(new Error('Invalid token'));
         } else {
            //   console.log("Valid Token.");
              callback(null, decodedJwt);
         }
    })
    return decodedJwt;
};