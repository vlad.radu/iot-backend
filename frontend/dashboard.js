document.addEventListener('DOMContentLoaded', init);
var tempChart;
var wifiChart;
var rooms = {};
var wifiStrengthData = {}
var crtRomm; 
var accountData;

var chartList = {};

function get(url,callback) {
    $.ajax({
        url: url,
        type: "GET",
        beforeSend: function(req) {
            req.setRequestHeader("Token",getCookie("sessionToken"));
          },
        dataType: 'json'
    }).done((data, status, xhr) => {
        return callback(null, data, status);
    }).fail((data, status, xhr) => {
        console.log(data, status, xhr);
        return callback(status);
    } );
}

function post(url, data, callback) {
    $.ajax({
        url: url,
        type: "POST",
        beforeSend: function(req) {
            req.setRequestHeader("Token",getCookie("sessionToken"));
          },
        dataType: 'json',
        data: data
    }).done((data, status, xhr) => {
        return callback(null, data, status);
    }).fail((data, status, xhr) => {
        console.log(data, status, xhr);
        return callback(status);
    } );
}

function initTemp() {
    get("/api/temp", (err, data, status) => {
        console.log(data);
        data.forEach( d => {
            add(d);
        })
    });
}

function add(data) {
    // console.log(data);
    axisData = dataset = chartList[data.id].data;
    dataset = axisData.datasets[0];
    dataset.data.push(data.temp);
    axisData.labels.push(new Date(parseInt(data.timestamp)));
    if (dataset.data.length > 50 ){
        dataset.data = dataset.data.slice(-1,1);
        axisData.labels = axisData.labels.slice(-1,1);
    }
    chartList[data.id].update();
    document.getElementById(`${data.id}-upAt`).textContent = new Date().toTimeString();
}

function parseData(d){
    switch(d.event) {
      case "temp":
          break;
      case "led":
          break;
      case "prox":
          break;
      default:
          break; 
    }
  } 

// function setPollingRate(){
//     let pollRate = parseFloat(document.getElementById("pollRate").value);
//     if( typeof pollRate !== "number" ){
//         console.log("bad val");
//         return;
//     }
//     console.log(pollRate);
//     // post poll rate
// }

function main() {
    
    $("#logout").click(function() {
        clearCookie('sessionToken');
        redirectSameLevel('/');
    });
    $("#user").text(localStorage.getItem("username"));
    let wsc = new WebSocket('ws://localhost:50000', [getCookie("sessionToken")]);
    wsc.addEventListener("open", (data) => {
        console.log(data);
    });
    wsc.addEventListener("message", (data) => {
        console.log(data);
        let d = JSON.parse(data.data);
        add(d);
    });
    get("/api/status", (err, data) => {
        if (err || !data.devices) {
            console.log("init Failed!");
        }
        console.log(data);
        acountData = data;
        data.devices.forEach( device => {
            chartList[device] = addChartToContainer("#mainContainer", device); 
        });
    });
    initTemp();
}

function addChartToContainer(container, deviceName) {
    template = `                    
        <div class="col-md-6">
            <div class="card">
                <div class="header">
                    <h4 class="title">${deviceName}</h4>
                </div>
                <div class="content">
                    <canvas id="${deviceName}-chart"></canvas>
                    <div class="footer">
                        <hr>
                        <div class="stats">
                            <i class="fa fa-history"></i> Updated at <span id="${deviceName}-upAt"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>`
    isAdded = false;
    containerChildren = $(container).children();
    Object.entries(containerChildren).forEach(child => {
        if (child[1].children && child[1].children.length < 2){
            $(child[1]).append(template);
            console.log("here");
            isAdded = true;
        }
    });
    if(!isAdded) {
        console.log("second here");
        $(container).append(`<div class="row">
            ${template}
        </div>`);
        console.log("done");
    }
    let axisData = {};
    axisData.labels = [];
    axisData.datasets = [];
    axisData.datasets.push({label: "temp", borderColor:"orange", backgroundColor: "rgba(252, 186, 3, 0.3", data: []});

    let ctx = document.getElementById(`${deviceName}-chart`).getContext('2d');
    ctx.canvas.width = 600;
    ctx.canvas.height = 200;
    return new Chart(ctx, {
        type: 'line',
        data: axisData,
        options: {
            scales: {
                xAxes: [{
                    display: true,
                    type: 'time',
                    time: {
                      parser: 'MM/DD/YYYY HH:mm',
                      tooltipFormat: 'll HH:mm',
                      displayFormats: {
                        'day': 'MM/DD/YYYY'
                      }
                    }
                }],
                yAxes: [{
                    ticks: {
                        suggestedMax: 40
                    },
                    stacked: true
                }]
            }
        }
    });
}

function init() {
    let sessionCheck = getCookie("sessionToken");
    if (sessionCheck){
        let pack = {
            token: sessionCheck
        }
        $.ajax({
            url: "/api/validate",
            type: "POST",
            data: JSON.stringify(pack),
            dataType: "json"
        }).done((data, status, xhr) => {
            main();
        }).fail((data, status, xhr) => {
            console.log("failed with", xhr);
            redirectSameLevel('/');
        });
    }else {
        redirectSameLevel('/');
    }
}
