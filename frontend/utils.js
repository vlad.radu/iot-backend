function getCookie(name) {
    let dc = document.cookie;
    let prefix = name + "=";
    let begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else
    {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
        end = dc.length;
        }
    }
    return decodeURI(dc.substring(begin + prefix.length, end));
}

function clearCookie(name){
    // var domain = domain || document.domain;
    // var path = path || "/";
    document.cookie = name + "=; expires=" + +new Date;
};

function redirectSameLevel(url) {
    let location = window.location.pathname;
    location = location.substring(0, location.lastIndexOf('/'));
    location += url.indexOf('/') === 0 ? url : `/${url}`;
    window.location.replace(location);
}

function setCookie(key, value) {
    document.cookie = `${key}=${value}`;
}