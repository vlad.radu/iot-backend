const awsIot = require('aws-iot-device-sdk');
const websocket = require("ws");
const http = require("http");
const aws = require("aws-sdk");
const awssdk = require("./config").awssdk;
const awsiot = require("./config").awsiot;
const login = require("./auth").Login;
const validate = require("./auth").Validate;
const syncScan = require("./clientManager").scan;
const CManager = require("./clientManager").ClientManager;

var device = awsIot.device(awsiot);

const PORT = 50000;
const DEVICE_TABLE = "iot";
const USER_TABLE = "iotusers";
const TOKEN_USER_KEY = "cognito:username";

aws.config.update( awssdk );

let docClient = new aws.DynamoDB.DocumentClient();

let clientManager = new CManager(docClient);

/*
 Topic structure:
  <device>/<location>/<locationId>/<type>/<id>
*/

device.on('connect', function() {
  console.log('connect');
  device.subscribe('#', (err,data) => {
      if (err) {
        console.log(err);
      }
      console.log(data);
  });
  device.on('message', (topic, data) => {
    console.log(topic);
    let split = topic.split('/');
    console.log(split);
    let recv = JSON.parse(data.toString());
    switch(split[1]) {
      case "temp":
          clientManager.forEach( user => {
            if (user.devices.includes(split[0])){
              user.sockets.forEach((s) => {
                try {
                  s.send(JSON.stringify(recv));
                } catch(e) {
                  s.terminate();
                  console.log(err);
                }
              })
            }
            
          });
        break;
      default:
        console.log("Unhandled topic" + topic);
        break;
    }

});
});


function readBody(req, callback) {
  let body = "";
  req.on("data", (data) => {
    body += data.toString();
  }).on("end", () =>{
    let ret = "";
    try {
      ret = JSON.parse(body);
    } catch(e){
      console.log(e);
      return callback(e);
    }
    return callback(null, ret);
  })
}

function reject(res) {
  res.writeHead(401, {"Content-Type": "application/json"});
  res.write(JSON.stringify({status: "error"}));
  res.end();
}

function sendOk(res) {
  res.writeHead(200, {"Content-Type": "application/json"});
  res.write(JSON.stringify({status: "ok"}));
  res.end();
}

async function getTemps(jwtData, res) {
  let d = new Date();
  d.setHours(d.getHours() - 24);

  client = clientManager.get(jwtData.payload[TOKEN_USER_KEY]);
  
  if(!client) {
    clientManager.saveClient(jwtData);
  }
  
  let devices = clientManager.get(jwtData.payload[TOKEN_USER_KEY]).devices
  let filterExpr = "";
  let exprAttr = {};
  devices.forEach( dev => {
    filterExpr += `id = :${dev} or `;
    exprAttr[`:${dev}`] = dev;
  });
  filterExpr = "(" + filterExpr.slice(0, filterExpr.lastIndexOf("or"));
  filterExpr += ") and #timestamp > :time";
  exprAttr[":time"] = +d;
  let params = {
    TableName: DEVICE_TABLE,
    FilterExpression : filterExpr,
    ExpressionAttributeNames: { "#timestamp": "timestamp" },
    ExpressionAttributeValues : exprAttr
  }
  let data = await syncScan(docClient, params);
  res.writeHead(200, {"Content-Type": "application/json"});
  res.write(JSON.stringify(data.Items));
  res.end();
}

function authenticate(req, res) {
  readBody(req, (err, ret) => {
    if (err){
      res.writeHead(500);
      res.end();
      return;
    }
    login(ret, (err, token) => {
      if (err) {
        console.log(err);
        reject(res);
        return;
      };
      validate(token.getJwtToken(), (err, jwtData) => {
        if (err) {
          console.log(err);
          reject(res);
          return;
        }
        console.log(149);
        clientManager.saveClient(jwtData);
        console.log("validated from", req.connection.remoteAddress);
        res.writeHead(200, {"Content-Type": "application/json"});
        res.write(JSON.stringify({sessionToken: token.getJwtToken(),
                                  username: token.payload[TOKEN_USER_KEY]}));
        res.end();
      });
    });
  });
}
// -------- main exec 

// getStatus();

const server = http.createServer(50000, function (req, res) {
  console.log(req.url);
  if (req.method === 'POST'){
    switch(req.url){
      case '/auth':
        authenticate(req, res);
        break;
      case '/validate':
        readBody(req, (err, data)=> {
          if(err){
            console.log(err);
            reject(res);
            return;
          }
          validate(data.token, async (err, jwt) => {
            if (err) {
              console.log(err);
              reject(res);
              return;
            }
            console.log(186);
            if (!clientManager.get(jwt.payload[TOKEN_USER_KEY]))
              await clientManager.saveClient(jwt);
            else
              await clientManager.updateClientData(jwt.payload[TOKEN_USER_KEY]);
            sendOk(res);
            return;
          });
        });
        break;
      default:
        break;
    }
  }else if(req.method === 'GET') {
    if (!req.headers.token){
      reject(res);
      console.log("rejected!");
      return;
    }
    validate(req.headers.token, (err, jwt) => {
      if (err) {
        reject(res);
        return;
      }
      console.log(214);
      if (!clientManager.get(jwt.payload[TOKEN_USER_KEY]))
        clientManager.saveClient(jwt);
      else
        clientManager.updateClientData(jwt.payload[TOKEN_USER_KEY]);
      
        switch(req.url){
        case '/status':
            console.log(222);
            // if (!clientManager.get(jwt.payload[TOKEN_USER_KEY])){
            //   clientManager.saveClient(jwt);
            // }
            devices = clientManager.get(jwt.payload[TOKEN_USER_KEY]).devices;
            clientObj = { devices: devices };
            res.writeHead(200, {"Content-Type": "application/json"});
            res.write(JSON.stringify(clientObj));
            res.end();
          break;
        case '/temp':
          getTemps(jwt, res);
          break;
        default:
          break;
      }
    });
  }
});
const wss = new websocket.Server({ server });


wss.on('connection', function (ws, req) {
  console.log("New WebSocket connection from " + req.connection.remoteAddress);
  // console.log(req.headers['sec-websocket-protocol']);
  validate(req.headers['sec-websocket-protocol'], (err, jwtData) => {
  if (err) {
    console.log(err);
    ws.terminate();
    return;
  }
  clientManager.saveClient(jwtData, ws);
  });

});

server.listen(PORT);

console.log("Listening on port " + PORT);

